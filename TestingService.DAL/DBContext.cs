﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using TestingService.DAL.Entities;

namespace TestingService.DAL
{
    public class DBContext : IdentityDbContext<UserAccount, IdentityRole<Guid>, Guid>
    {
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Result> Results { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            var admin_r = new IdentityRole<Guid>("Admin")
            {
                Id = Guid.Parse("d3fb6d02-d48d-4356-a596-4f7398d3d9a8")
            };
            var user_r = new IdentityRole<Guid>("User")
            {
                Id = Guid.Parse("32d82b43-7cb9-4941-8442-40cf15a270d8")
            };
            modelBuilder.Entity<IdentityRole<Guid>>().HasData(admin_r, user_r);

            var admin = new UserAccount()
            {
                Id = Guid.Parse("13d48de3-8ff2-470c-8759-c673469575da"),
                UserName = "Admin",
                NormalizedUserName = "ADMIN",
                Email = "admin@admin.com",
                NormalizedEmail = "ADMIN@ADMIN.COM",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            var user = new UserAccount()
            {
                Id = Guid.Parse("099e1d12-8360-4c2e-a2fa-82130e3663f6"),
                UserName = "User",
                NormalizedUserName = "USER",
                Email = "user@user.com",
                NormalizedEmail = "USER@USER.COM",
                SecurityStamp = Guid.NewGuid().ToString()
            };
            PasswordHasher<UserAccount> passwordHasher = new PasswordHasher<UserAccount>();
            var passh_admin = passwordHasher.HashPassword(admin, "ADMIN_1_admin");
            var passh_user = passwordHasher.HashPassword(user, "USER_1_user");
            admin.PasswordHash = passh_admin;
            user.PasswordHash = passh_user;
            modelBuilder.Entity<UserAccount>().HasData(admin, user);

            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(
            new IdentityUserRole<Guid>
                {
                    RoleId = Guid.Parse("32d82b43-7cb9-4941-8442-40cf15a270d8"),
                    UserId = Guid.Parse("099e1d12-8360-4c2e-a2fa-82130e3663f6")
                },
            new IdentityUserRole<Guid>
                {
                    RoleId = Guid.Parse("d3fb6d02-d48d-4356-a596-4f7398d3d9a8"),
                    UserId = Guid.Parse("13d48de3-8ff2-470c-8759-c673469575da")
                }
            );
            base.OnModelCreating(modelBuilder);
        }
    }
}