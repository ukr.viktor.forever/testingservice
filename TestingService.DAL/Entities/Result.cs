﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingService.DAL.Entities
{
    public class Result
    {
        public int Id { get; set; }
        public int CorrectAnswers { get; set; }
        public Test Test  { get; set; }
        public UserAccount User { get; set; }
    }
}
