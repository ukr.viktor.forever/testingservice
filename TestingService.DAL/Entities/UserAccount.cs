﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace TestingService.DAL.Entities
{
    public class UserAccount : IdentityUser<Guid>
    {
        public ICollection<Result> Results { get; set; }
        public ICollection<Test> Tests { get; set; }
    }
}
