﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingService.DAL.Entities
{
    public class Answer
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public bool isCorrect { get; set; }
    }
}
