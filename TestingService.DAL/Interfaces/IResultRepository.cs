﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.DAL.Entities;

namespace TestingService.DAL.Interfaces
{
    public interface IResultRepository
    {
        Task AddAsync(Result entity);
        Task DeleteAsync(Result entity);
        Task<Result> GetAsync(int id);
        Task<IEnumerable<Result>> GetByUserAsync(Guid userId);
        Task<IEnumerable<Answer>> GetAnswersByIdsAsync(IEnumerable<Guid> ids);
        Task UpdateAsync(Result entity);
    }
}
