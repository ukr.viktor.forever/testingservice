﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.DAL.Entities;

namespace TestingService.DAL.Interfaces
{
    public interface ITestRepository
    {
        Task AddAsync(Test entity);
        Task DeleteAsync(Test entity);
        Task<Test> GetAllAsync(int id);
        Task<Test> GetAsync(int id);
        Task<IEnumerable<Test>> GetByUserAsync(UserAccount User);

        Task UpdateAsync(Test entity);
    }
}
