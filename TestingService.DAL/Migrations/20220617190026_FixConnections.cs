﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TestingService.DAL.Migrations
{
    public partial class FixConnections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Tests_TestId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "CorrectAnswers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_TestId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TestId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<bool>(
                name: "isCorrect",
                table: "Answers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "TestUserAccount",
                columns: table => new
                {
                    AllowedUsersId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TestsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestUserAccount", x => new { x.AllowedUsersId, x.TestsId });
                    table.ForeignKey(
                        name: "FK_TestUserAccount_AspNetUsers_AllowedUsersId",
                        column: x => x.AllowedUsersId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestUserAccount_Tests_TestsId",
                        column: x => x.TestsId,
                        principalTable: "Tests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TestUserAccount_TestsId",
                table: "TestUserAccount",
                column: "TestsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TestUserAccount");

            migrationBuilder.DropColumn(
                name: "isCorrect",
                table: "Answers");

            migrationBuilder.AddColumn<int>(
                name: "TestId",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CorrectAnswers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    QuestionId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CorrectAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CorrectAnswers_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_TestId",
                table: "AspNetUsers",
                column: "TestId");

            migrationBuilder.CreateIndex(
                name: "IX_CorrectAnswers_QuestionId",
                table: "CorrectAnswers",
                column: "QuestionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Tests_TestId",
                table: "AspNetUsers",
                column: "TestId",
                principalTable: "Tests",
                principalColumn: "Id");
        }
    }
}
