﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TestingService.DAL.Migrations
{
    public partial class CorrectAnswersField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("c5b87e02-eac7-4286-8013-c2b7f00a9424"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d0115b4d-b54a-46bf-9434-23cba66c8b2b"));

            migrationBuilder.CreateTable(
                name: "CorrectAnswers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    QuestionId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CorrectAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CorrectAnswers_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_CorrectAnswers_QuestionId",
                table: "CorrectAnswers",
                column: "QuestionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CorrectAnswers");

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TestId", "TwoFactorEnabled", "UserName" },
                values: new object[] { new Guid("c5b87e02-eac7-4286-8013-c2b7f00a9424"), 0, "c022599e-5d54-4035-8340-2e5598cfde8a", "admin@admin.com", false, false, null, null, null, "AQAAAAEAACcQAAAAEDoz7Gg+hcSY091bBG5YUkEiSQ7ryJpE/UdFGLnR8kfUkxNK0HB78WnWZhqmvZvv4A==", null, false, null, null, false, "admin" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TestId", "TwoFactorEnabled", "UserName" },
                values: new object[] { new Guid("d0115b4d-b54a-46bf-9434-23cba66c8b2b"), 0, "36c49a6b-ed9e-4543-8c37-86e940aa3fa8", "user@user.com", false, false, null, null, null, "AQAAAAEAACcQAAAAEOVaSAzPwvLilUvU+4a5g9Ir6EMwRZIcIa1BbN9MsGADKkJkCa71fPPyuNuba5wYEg==", null, false, null, null, false, "user" });
        }
    }
}
