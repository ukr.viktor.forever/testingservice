﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TestingService.DAL.Migrations
{
    public partial class DBSeedUsersAdding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("32d82b43-7cb9-4941-8442-40cf15a270d8"), "61b2e54b-5585-4082-9802-9ac8d3f3e5d6", "User", null },
                    { new Guid("d3fb6d02-d48d-4356-a596-4f7398d3d9a8"), "7dc234dd-b565-4f00-bf36-d2ebf173940b", "Admin", null }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("099e1d12-8360-4c2e-a2fa-82130e3663f6"), 0, "d1454521-864a-413d-9411-2c45afaea1fc", "user@user.com", false, false, null, "USER@USER.COM", "USER", "AQAAAAEAACcQAAAAEOtMl7K283N2zN3yHUTF6dFHqMuVsVmW/xEdocAYpNRVvawvH1hbiHmi4tN7A+mgfg==", null, false, "f7012842-9920-46b9-9396-66fdcba17e87", false, "User" },
                    { new Guid("13d48de3-8ff2-470c-8759-c673469575da"), 0, "724c2468-8811-4e18-a94e-3948ca298349", "admin@admin.com", false, false, null, "ADMIN@ADMIN.COM", "ADMIN", "AQAAAAEAACcQAAAAEJitCt1ofdUA0kKY9OJNCRTOUaKCjGAILSHiwmJoDd2Kff0YMjfztq0Yya2ZRKlruQ==", null, false, "647def3c-68ad-4a04-aeac-cfb6d97e0612", false, "Admin" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { new Guid("32d82b43-7cb9-4941-8442-40cf15a270d8"), new Guid("099e1d12-8360-4c2e-a2fa-82130e3663f6") });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { new Guid("d3fb6d02-d48d-4356-a596-4f7398d3d9a8"), new Guid("13d48de3-8ff2-470c-8759-c673469575da") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("32d82b43-7cb9-4941-8442-40cf15a270d8"), new Guid("099e1d12-8360-4c2e-a2fa-82130e3663f6") });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("d3fb6d02-d48d-4356-a596-4f7398d3d9a8"), new Guid("13d48de3-8ff2-470c-8759-c673469575da") });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("32d82b43-7cb9-4941-8442-40cf15a270d8"));

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("d3fb6d02-d48d-4356-a596-4f7398d3d9a8"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("099e1d12-8360-4c2e-a2fa-82130e3663f6"));

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("13d48de3-8ff2-470c-8759-c673469575da"));
        }
    }
}
