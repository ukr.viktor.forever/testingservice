﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.DAL.Entities;
using TestingService.DAL.Interfaces;

namespace TestingService.DAL.Repositories
{
    public class TestRepository : ITestRepository
    {
        private readonly DBContext _context;

        public TestRepository(DBContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Test entity)
        {
            await _context.Tests.AddAsync(entity);
            _context.SaveChanges();
        }
        public async Task DeleteAsync(Test entity)
        {
            _context.Tests.Remove(entity);
            await _context.SaveChangesAsync();
        }
        public async Task<Test> GetAllAsync(int id)
        {
            return await _context.Tests.Include(x => x.Questions).ThenInclude(x => x.Answers).Where(x=>x.Id == id).FirstOrDefaultAsync();
        }
        public async Task<Test> GetAsync(int id)
        {
            return await _context.Tests.FindAsync(id);
        }
        public async Task<IEnumerable<Test>> GetByUserAsync(UserAccount User)
        {
            return await _context.Tests.Where(x => x.AllowedUsers.Contains(User)).ToListAsync();
        }
        public async Task UpdateAsync(Test entity)
        {
            _context.Tests.Update(entity);
            await _context.SaveChangesAsync();
        }
    }
}
