﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.DAL.Entities;
using TestingService.DAL.Interfaces;

namespace TestingService.DAL.Repositories
{
    public class ResultRepository : IResultRepository
    {
        private readonly DBContext _context;

        public ResultRepository(DBContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Result entity)
        {
             await _context.Results.AddAsync(entity);
            _context.SaveChanges();
        }

        public async Task DeleteAsync(Result entity)
        {
            _context.Results.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<Result> GetAsync(int id)
        {
            return await _context.Results.FindAsync(id);
        }

        public async Task<IEnumerable<Answer>> GetAnswersByIdsAsync(IEnumerable<Guid> ids)
        {
            //_dataContext.UserProfile.Where(t => idList.Contains(t.Id));
            return await _context.Answers.Where(x=> ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<Result>> GetByUserAsync(Guid userId)
        {
            return await _context.Results.Where(x => x.User.Id == userId).Include(x => x.Test).ToListAsync();
        }

        public async Task UpdateAsync(Result entity)
        {
            _context.Results.Update(entity);
            await _context.SaveChangesAsync();
        }
    }
}
