﻿using AutoMapper;
using System.Linq;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;

namespace TestingService.API
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserAccount, UserRegisterModel>().ReverseMap();
            CreateMap<Test, TestViewModel>().ReverseMap();
            CreateMap<Test, TestingModel>();

            CreateMap<Question, QuestionModel>()
                .ForMember(dst => dst.AnswersText, opt => opt.MapFrom(srs => srs.Answers.Select(x => x.Text)))
                .ForMember(dst => dst.AnswerIds, opt => opt.MapFrom(srs => srs.Answers.Select(x => x.Id.ToString())));

            CreateMap<Test, TestingModel>()
                .ForMember(srs=>srs.Id, opt=> opt.MapFrom(srs=>srs.Id))
                .ForMember(srs => srs.Questions, opt => opt.MapFrom(srs => srs.Questions));

            CreateMap<Result, ResultModel>()
                .ForMember(dst => dst.TestName, opt => opt.MapFrom(x => x.Test.Name))
                .ForMember(dst => dst.TestDescription, opt => opt.MapFrom(x => x.Test.Description));

            CreateMap<QuestionAddingModel, Question>()
                .ForMember(dst => dst.Answers, opt => opt.MapFrom(x => x.Answers))
                .ReverseMap();
            CreateMap<AnswerModel, Answer>().ReverseMap();
        }
    }
}
