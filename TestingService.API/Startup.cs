using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using TestingService.API;
using TestingService.API.Extensions;
using TestingService.BLL.Settings;
using TestingService.BLL.Interfaces;
using TestingService.BLL.Services;
using TestingService.DAL;
using TestingService.DAL.Entities;
using TestingService.DAL.Repositories;
using TestingService.DAL.Interfaces;

namespace TestingService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.WriteIndented = true);

            services.AddDbContext<DBContext>(config =>
            {
                config.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                    optionsBuilder =>
                    {
                        optionsBuilder.MigrationsAssembly("TestingService.DAL");
                    });
            });

            services.AddIdentity<UserAccount, IdentityRole<Guid>>(config =>
            {
                config.User.RequireUniqueEmail = true;
            })
           .AddEntityFrameworkStores<DBContext>()
           .AddDefaultTokenProviders();

            services.AddScoped<ITestRepository, TestRepository>();
            services.AddScoped<IResultRepository, ResultRepository>();

            services.AddTransient<ITestService, TestService>();
            services.AddTransient<IResultService, ResultService>();
            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IUserService, UserService>();

            services.AddAutoMapper(typeof(AutoMapperProfile));

            services.Configure<JwtSettings>(Configuration.GetSection("Jwt"));
            var jwtSettings = Configuration.GetSection("Jwt").Get<JwtSettings>();
            services.AddAuth(jwtSettings);

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
