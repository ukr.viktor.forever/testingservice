﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestingService.BLL.Interfaces;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;

namespace TestingService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ITestService _service;
        private readonly UserManager<UserAccount> _userManager;

        public TestController(ITestService service, UserManager<UserAccount> userManager)
        {
            _service = service;
            _userManager = userManager;
        }



        [HttpGet("{UserId}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<TestViewModel>>> GetMyTests(string UserId)
        {
            var tests = await _service.GetForUserAsync(Guid.Parse(UserId));
            if (tests == null)
            {
                return NotFound();
            }
            return Ok(tests);
        }


        [HttpGet("start/{testId}")]
        [Authorize]
        public async Task<ActionResult<TestingModel>> GetTesting(int testId)
        {
            var test = await _service.GetTesting(testId);
            if(test == null)
            {
                return NotFound();
            }
            return Ok(test);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> AddTest(TestModel test)
        {
            if(test == null || test.Questions.Count == 0 || test.AllowedUsers.Count == 0)
            {
                return BadRequest();
            }
            return Ok(await _service.AddAsync(test));
        }


    }
}
