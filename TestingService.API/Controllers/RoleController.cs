﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;

namespace TestingService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;
        private readonly UserManager<UserAccount> _manager;
        public RoleController(RoleManager<IdentityRole<Guid>> roleManager, UserManager<UserAccount> manager)
        {
            _roleManager = roleManager;
            _manager = manager;
        }

        [HttpPut]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddUserToRole([FromBody]AddUserToRoleModel data)
        {
            var user = await _manager.FindByNameAsync(data.Name);
            if (user == null)
                return NotFound(data.Name);
            if (!await _roleManager.RoleExistsAsync(data.Role))
                return NotFound(data.Role);
            var result = await _manager.AddToRoleAsync(user, data.Role);
            if (!result.Succeeded)
                return BadRequest(result.Errors.First().Description);
            else
                return Ok();
        }
        //[Authorize(Roles = "Admin")]
        //[HttpGet("CreateRole/{roleName}")]
        //public async Task<IActionResult> CreateRole(string roleName)
        //{
        //    if (string.IsNullOrWhiteSpace(roleName))
        //    {
        //        return BadRequest("Role name should be provided.");
        //    }

        //    var newRole = new IdentityRole<Guid>
        //    {
        //        Name = roleName
        //    };

        //    var roleResult = await _roleManager.CreateAsync(newRole);

        //    if (roleResult.Succeeded)
        //    {
        //        return Ok();
        //    }

        //    return Problem(roleResult.Errors.First().Description, null, 500);
        //}
    }
}
