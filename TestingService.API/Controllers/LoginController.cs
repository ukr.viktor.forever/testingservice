﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using TestingService.BLL.Interfaces;
using TestingService.BLL.Models;
using TestingService.BLL.Settings;
using TestingService.DAL.Entities;

namespace TestingService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class LoginController : ControllerBase
    {
        protected readonly IAuthService service;
        protected readonly UserManager<UserAccount> _userManager;
        protected readonly IOptions<JwtSettings> _jwtSettings;
        protected readonly IMapper _mapper;

        public LoginController(UserManager<UserAccount> userManager, IAuthService service, IOptions<JwtSettings> options, IMapper mapper)
        {
            _userManager = userManager;
            _jwtSettings = options;
            _mapper = mapper;
            this.service = service;
        }

        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(UserLoginModel userModel)
        {
            var result = await service.SignIn(userModel);
            if (!result.Succeeded)
            {
                return Unauthorized(result.Errors);
            }
            var JWT = await service.GenerateJwt(userModel);
            return Ok(new { access_token = JWT });

        }

        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUp(UserRegisterModel model)
        {
            var result = await service.SignUp(model);
            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }
            return StatusCode(201);
        }


    }
}
