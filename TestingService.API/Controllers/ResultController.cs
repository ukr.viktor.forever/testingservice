﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestingService.BLL.Interfaces;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;

namespace TestingService.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultController : ControllerBase
    {
        private readonly IResultService _service;
        private readonly UserManager<UserAccount> _userManager;

        public ResultController(IResultService service, UserManager<UserAccount> userManager)
        {
            _service = service;
            _userManager = userManager;
        }
        [HttpPost("{testId}/{userId}")]
        [Authorize]
        public async Task<ActionResult<int>> RegisterResult([FromBody] IEnumerable<string> result,int testId, string userId)
        {
            int res = 0;
            if (result == null || testId == default(int) || userId == null)
            {
                return BadRequest();
            }
            var user = await _userManager.FindByIdAsync(userId);
            res = await _service.AddAsync(result, testId, user);
            return Ok(res);
        }

        [HttpGet("{userId}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ResultModel>>> GetUserResults(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            return Ok(await _service.GetResultsByUser(user));
        }

    }
}
