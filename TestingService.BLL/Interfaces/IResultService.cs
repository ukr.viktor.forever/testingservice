﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;

namespace TestingService.BLL.Interfaces
{
    public interface IResultService
    {
        public Task<int> AddAsync(IEnumerable<string> result, int testId, UserAccount user);
        public Task<IEnumerable<ResultModel>> GetResultsByUser(UserAccount user);
    }
}
