﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.BLL.Models;

namespace TestingService.BLL.Interfaces
{
    public interface IUserService
    {
        public Task<IEnumerable<string>> GetUserNames();
        public Task<IEnumerable<UserViewModel>> Get();
    }
}
