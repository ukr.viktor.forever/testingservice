﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.BLL.Models;

namespace TestingService.BLL.Interfaces
{
    public interface ITestService
    {
        public Task<IEnumerable<TestViewModel>> GetForUserAsync(Guid UserId);
        public Task<TestingModel> GetTesting(int TestId);
        public Task<IAsyncResult> AddAsync(TestModel testModel);

    }
}
