﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.BLL.Models;

namespace TestingService.BLL.Interfaces
{
    public interface IAuthService
    {
        public Task<IdentityResult> SignIn(UserLoginModel userModel);
        public Task<IdentityResult> SignUp(UserRegisterModel model);
        public Task<string> GenerateJwt(UserLoginModel userModel);
        
    }
}
