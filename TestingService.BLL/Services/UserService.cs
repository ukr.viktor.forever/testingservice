﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestingService.BLL.Interfaces;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;

namespace TestingService.BLL.Services
{
    public class UserService : IUserService
    {
        protected readonly UserManager<UserAccount> _userManager;
        public UserService(UserManager<UserAccount> userManager)
        {
            _userManager = userManager;
        }
        public async Task<IEnumerable<string>> GetUserNames()
        {
            return await _userManager.Users.Select(x => x.UserName).ToListAsync();
        }
        public async Task<IEnumerable<UserViewModel>> Get()
        {
            List<UserViewModel> result = new List<UserViewModel>();
            var users = await _userManager.Users.ToListAsync();
            foreach (var user in users)
            {
                result.Add(new UserViewModel()
                {
                    Email = user.Email,
                    UserName = user.UserName,
                    Roles = await _userManager.GetRolesAsync(user)
                });
            }
            return result;
        }
    }
}
