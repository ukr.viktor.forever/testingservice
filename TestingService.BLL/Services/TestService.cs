﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestingService.BLL.Interfaces;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;
using TestingService.DAL.Interfaces;


namespace TestingService.BLL.Services
{
    public class TestService : ITestService
    {
        private readonly UserManager<UserAccount> _userManager;
        private readonly ITestRepository _repository;
        private readonly IMapper _mapper;

        public TestService(ITestRepository repository, UserManager<UserAccount> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<TestViewModel>> GetForUserAsync(Guid UserId)
        {
            if (UserId == Guid.Empty)
            {
                return null;
            }
            var user = await _userManager.FindByIdAsync(UserId.ToString());
            if (user == null)
            {
                return null;
            }
            var tests = (await _repository.GetByUserAsync(user)).ToList();
            return _mapper.Map<IEnumerable<Test>, IEnumerable<TestViewModel>>(tests);
        }
        public async Task<TestingModel> GetTesting(int testId)
        {
            var test = await _repository.GetAllAsync(testId);
            if (test == null)
            {
                return null;
            }
            var testModel = _mapper.Map<Test, TestingModel>(test);
            return testModel;
        }
        public async Task<IAsyncResult> AddAsync(TestModel testModel)
        {
            //var users = testModel.AllowedUsers.Select(x => _userManager.FindByNameAsync(x).Result).ToList();
            var users = await _userManager.Users.Where(x => testModel.AllowedUsers.Contains(x.UserName)).ToListAsync();
            Test test = new Test()
            {
                AllowedUsers = users,
                Description = testModel.Description,
                Name = testModel.Name,
                Questions = (ICollection<Question>)_mapper.Map<IEnumerable<QuestionAddingModel>, IEnumerable<Question>>(testModel.Questions)

            };
            return _repository.AddAsync(test);
        }
    }
}
