﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;
using TestingService.BLL.Interfaces;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using TestingService.BLL.Settings;
using System.Net;


namespace TestingService.BLL.Services
{
    public class AuthService : IAuthService
    {
        protected readonly UserManager<UserAccount> _userManager;
        protected readonly IOptions<JwtSettings> _jwtSettings;
        protected readonly IMapper _mapper;
        public AuthService(UserManager<UserAccount> userManager, IOptions<JwtSettings> options, IMapper mapper)
        {
            _userManager = userManager;
            _jwtSettings = options;
            _mapper = mapper;
        }
        public async Task<IdentityResult> SignIn(UserLoginModel userModel)
        {
            var user = await _userManager.FindByEmailAsync(userModel.Email);
            if (user is null)
            {
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = $"{HttpStatusCode.NotFound}",
                    Description = $"User with email = \"{userModel.Email}\" wasn`t found"
                });
            }
            var SignInResult = await _userManager.CheckPasswordAsync(user, userModel.Password);
            if (!SignInResult)
            {
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = $"{HttpStatusCode.BadRequest}",
                    Description = $"Wrong password !"
                });
            }
            return IdentityResult.Success;
        }
        public async Task<IdentityResult> SignUp(UserRegisterModel model)
        {

            var user = _mapper.Map<UserRegisterModel, UserAccount>(model);
            var userCreateResult = await _userManager.CreateAsync(user, model.Password);
            var userAddToRoleResult = await _userManager.AddToRoleAsync(user, "User");
            if (!userCreateResult.Succeeded)
            {
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = $"{HttpStatusCode.InternalServerError}",
                    Description = userCreateResult.Errors.ToString()
                });
            }
            if (!userAddToRoleResult.Succeeded)
            {
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = $"{HttpStatusCode.InternalServerError}",
                    Description = userAddToRoleResult.Errors.ToString()
                });
            }
            return IdentityResult.Success;


        }
        public async Task<string> GenerateJwt(UserLoginModel userModel)
        {
            var user = await _userManager.FindByEmailAsync(userModel.Email);
            var roles = await _userManager.GetRolesAsync(user);
            var jwtSettings = _jwtSettings.Value;
            var securityKey = jwtSettings.GetSymmetricSecurityKey();
            var cregentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddSeconds(jwtSettings.TokenLifeTime);
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
            };
            var roleClaims = roles.Select(r => new Claim("role", r));
            claims.AddRange(roleClaims);
            var token = new JwtSecurityToken(
                jwtSettings.Issuer,
                audience: jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: cregentials
                );
            var tok = new JwtSecurityTokenHandler().WriteToken(token);
            return tok;
        }

    }
}
