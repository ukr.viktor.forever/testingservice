﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestingService.BLL.Interfaces;
using TestingService.BLL.Models;
using TestingService.DAL.Entities;
using TestingService.DAL.Interfaces;

namespace TestingService.BLL.Services
{
    public class ResultService : IResultService
    {
        private readonly IResultRepository _repository;
        private readonly ITestRepository _testRepository;
        private readonly IMapper _mapper;
        public ResultService(IResultRepository repository, ITestRepository testRepository, IMapper mapper)
        {
            _repository = repository;
            _testRepository = testRepository;
            _mapper = mapper;
        }
        public async Task<int> AddAsync(IEnumerable<string> results, int testId, UserAccount user)
        {

            int CorrectAnswersCount = 0;
            var guidRes = results.Select(x => Guid.Parse(x));
            var answers = await _repository.GetAnswersByIdsAsync(guidRes);
            foreach(var answer in answers)
            {
                if (answer.isCorrect)
                    CorrectAnswersCount++;
            }
            var test = await _testRepository.GetAsync(testId);
            var testResult = new Result()
            {
                CorrectAnswers = CorrectAnswersCount,
                Test = test,
                User = user

            };
            await _repository.AddAsync(testResult);
            return CorrectAnswersCount;

        }
        public async Task<IEnumerable<ResultModel>> GetResultsByUser(UserAccount user)
        {
            var results = await _repository.GetByUserAsync(user.Id);
            return _mapper.Map<IEnumerable<Result>,IEnumerable<ResultModel>>(results);
        }
    }
}
