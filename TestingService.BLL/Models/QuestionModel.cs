﻿using System.Collections.Generic;

namespace TestingService.BLL.Models
{
    public class QuestionModel
    {
        public string Text { get; set; }
        public ICollection<string> AnswersText  { get; set; }
        public ICollection<string> AnswerIds { get; set; }
    }
}
