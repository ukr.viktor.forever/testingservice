﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingService.BLL.Models
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public ICollection<string> Roles { get; set; }
        public string Email { get; set; }
    }
}
