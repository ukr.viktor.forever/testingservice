﻿using System;
using System.Collections.Generic;

namespace TestingService.BLL.Models
{
    public class ResultModel
    {
        public int CorrectAnswers { get; set; }
        public string TestName  { get; set; }
        public string TestDescription { get; set; }
    }
}
