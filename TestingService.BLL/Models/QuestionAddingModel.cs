﻿using System;
using System.Collections.Generic;

namespace TestingService.BLL.Models
{
    public class QuestionAddingModel
    {
        public string Text { get; set; }
        public ICollection<AnswerModel> Answers { get; set; }
    }
}
