﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingService.BLL.Models
{
    public class AddUserToRoleModel
    {
        public string Name { get; set; }
        public string Role { get; set; }
    }
}
