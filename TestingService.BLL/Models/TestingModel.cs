﻿using System.Collections.Generic;


namespace TestingService.BLL.Models
{
    public class TestingModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<QuestionModel> Questions { get; set; }
    }
}
