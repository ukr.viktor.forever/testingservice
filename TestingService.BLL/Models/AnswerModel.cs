﻿using System;
using System.Collections.Generic;

namespace TestingService.BLL.Models
{
    public class AnswerModel
    {
        public string Text { get; set; }
        public bool IsCorrect { get; set; }
    }
}
