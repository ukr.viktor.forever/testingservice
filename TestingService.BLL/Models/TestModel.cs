﻿using System;
using System.Collections.Generic;
namespace TestingService.BLL.Models
{
    public class TestModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<string> AllowedUsers { get; set; }
        public ICollection<QuestionAddingModel> Questions { get; set; }
    }
}
